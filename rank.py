# Rank a student on the basis of their marks, Number of subjects for each student will remain same

def read_marks(filename):
    marks = {}
    with open(filename, 'r') as file:
        for line in file:
            parts = line.split()
            if len(parts) < 4:
                continue
            student = parts[0]
            scores = [int(score) for score in parts[1:] if score.isdigit()]
            if len(scores) > 0:
                marks[student] = scores
    return marks

def compare_students(student1, student2, marks):
    scores1 = marks[student1]
    scores2 = marks[student2]
    for s1, s2 in zip(scores1, scores2):
        if s1 > s2:
            return 1
        elif s1 < s2:
            return -1
    return 0

def rank_students(marks):
    students = list(marks.keys())
    primary_rank = []
    secondary_candidates = []
    
    while students:
        current = students.pop(0)
        if not primary_rank:
            primary_rank.append(current)
        else:
            inserted = False
            for i, ranked_student in enumerate(primary_rank):
                comp_result = compare_students(current, ranked_student, marks)
                if comp_result == 1:
                    primary_rank.insert(i, current)
                    inserted = True
                    break
                elif comp_result == -1:
                    continue
                else:
                    if i == len(primary_rank) - 1:
                        secondary_candidates.append(current)
                        inserted = True
            if not inserted and current not in primary_rank:
                primary_rank.append(current)

    secondary_groups = []
    while secondary_candidates:
        current = secondary_candidates.pop(0)
        inserted = False
        for i, group in enumerate(secondary_groups):
            group_leader = group[0]
            comp_result = compare_students(current, group_leader, marks)
            if comp_result == 1:
                secondary_groups.insert(i, [current])
                inserted = True
                break
            elif comp_result == -1:
                continue
            else:
                if i == len(secondary_groups) - 1:
                    group.append(current)
                    inserted = True
        if not inserted:
            secondary_groups.append([current])

    return primary_rank, secondary_groups

def print_rankings(primary_rank, secondary_groups):
    return ">".join(primary_rank) + "\n" + "\n".join(f"Secondary Group {i+1}: <".join(group) for i, group in enumerate(secondary_groups) if group)


filename = 'studentRanking.txt' 
marks = read_marks(filename)
primary_rank, secondary_groups = rank_students(marks)
print(print_rankings(primary_rank, secondary_groups))
